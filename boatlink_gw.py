#!/usr/bin/env python3

# Copyright 2018 Jacques Supcik / HEIA-FR
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This program reads messages from the boat and publish them to redis.
# In a future version, it will also be able to send commands to the boat,
# but this is not yet implemented.
#

import json
import logging
import random
import time
from enum import Enum
from pathlib import Path

import click
import redis
import serial
from google.protobuf.json_format import MessageToJson

from heiafr.hydrocontest.feeder import state_machine
from heiafr.hydrocontest.proto.commande_pb2 import Commande_boat_t
from heiafr.hydrocontest.proto.information_pb2 import Information_boat_t

logger = logging.getLogger(__name__)


class Simulator:

    def __init__(self):
        self.txt = None
        self.i = 0
        self.ts = 0

    def create_random(self):
        now = time.time()
        if self.ts - now > 0:
            time.sleep(self.ts - now)
        self.ts = time.time() + 1
        msg = Information_boat_t()
        msg.Speed_boat = int(random.uniform(0, 300))
        msg.I_batterie = int(random.uniform(0, 600))
        msg.Position_foil1 = int(random.uniform(0, 2047))
        msg.Position_foil2 = int(random.uniform(0, 2047))
        msg.Cap_batterie_pourcent = int(random.uniform(0, 1000))
        msg.U_batterie = int(random.uniform(0, 250))
        msg.Speed_motor = int(random.uniform(0, 2000))
        msg.Temperature1 = int(random.uniform(0, 30000))
        msg.Error1 = random.choice([0,1,2,3,4])
        msg.Error2 = random.choice([0,6,7,8,9,10,11,12,13])

        body = msg.SerializeToString()
        crc = 0
        for c in body:
            crc ^= c
        self.txt = b"AAA" + (crc).to_bytes(1, 'little') + \
            (len(body)).to_bytes(1, 'little') + body + b"ZZ"
        self.i = 0

    def read(self):
        if self.txt is None or self.i >= len(self.txt):
            self.create_random()
        c = self.txt[self.i]
        self.i += 1
        return bytes([c])


class RedisWriter:

    def __init__(self, url=None):
        self.r = redis.Redis.from_url(url)

    def write(self, data):
        msg = Information_boat_t()
        msg.ParseFromString(data)
        res = json.loads(MessageToJson(msg))
        res['msgType'] = 'from-boat'
        logger.debug(json.dumps(res))
        self.r.publish("heiafr:hydrocontest:message", json.dumps(res))


@click.command()
@click.option('--debug', is_flag=True)
@click.option('--sim', is_flag=True)
@click.option('--port', default="/dev/ttyUSB0")
@click.option('--speed', default=115200)
@click.option('--redis', 'redis_url', default="redis://")
def main(debug, sim, port, speed, redis_url):
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if not Path(port).exists():
        logging.info("Port %s does not exist. Using simulation", port)
        sim = True

    if Path("/home/pi/simulate").exists():
        logging.info("Simulation forced by /home/pi/simulate")
        sim = True

    reader = Simulator() if sim else serial.Serial(port, speed)
    writer = RedisWriter(redis_url)

    sm = state_machine.StateMachine(reader, writer)
    sm.run()


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    main()
