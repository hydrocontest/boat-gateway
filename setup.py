#!/usr/bin/env python3

# Copyright 2018 Jacques Supcik / HEIA-FR
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Generate the python sources from the protoc buffer using this command:
# protoc -I=proto --python_out=heiafr/hydrocontest/proto proto/*.proto

from setuptools import setup

setup(
    name="heiafr-boatlink-gateway",
    version="1.0.6",
    packages=[
        "heiafr.hydrocontest.proto",
        "heiafr.hydrocontest.feeder",
    ],
    py_modules = ['boatlink_gw'],
    entry_points={
        'console_scripts': [
            'boatlink-gw = boatlink_gw:main',
        ],
    },
    author="Jacques Supcik",
    author_email="jacques.supcik@hefr.ch",
    description="Feeder for Hydrocontest 2018",
    license="Apache 2.0",
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
    ],
)
