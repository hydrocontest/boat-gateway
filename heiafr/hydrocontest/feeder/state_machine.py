# Copyright 2018 Jacques Supcik / HEIA-FR
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import time
from enum import Enum

#  +-+-+-+---+---++++++-+-+
#  |A|A|A|CRC|LEN|DATA|Z|Z|
#  +-+-+-+---+---++++++-+-+

logger = logging.getLogger(__name__)

class StateMachine:
    
    PROLOGUE_CHAR = ord(b'A')
    EPILOGUE_CHAR = ord(b'Z')
    PROLOGUE_LEN = 3
    EPILOGUE_LEN = 2
    SPEED_LIMIT = 0.1 # 100 ms

    class State(Enum):
        INIT = 1
        PROLOGUE = 2
        READ_CRC = 3
        READ_LEN = 4
        READ_BODY = 5
        EPILOGUE = 6

    stateMap = {
        State.INIT: "init",
        State.PROLOGUE: "prologue",
        State.READ_CRC: "read_crc",
        State.READ_LEN: "read_len",
        State.READ_BODY: "read_body",
        State.EPILOGUE: "epilogue",
    }

    def __init__(self, reader, writer):
        self.state = StateMachine.State.INIT
        self.count = 0
        self.msg_len = 0
        self.msg_crc = 0
        self.running_crc = 0
        self.body = bytearray()
        self.reader = reader
        self.writer = writer
        self.last_ts = 0
        
    def init(self, c):
        if c == StateMachine.PROLOGUE_CHAR:
            self.state = StateMachine.State.PROLOGUE
            self.count = 1

    def prologue(self, c):
        if c == StateMachine.PROLOGUE_CHAR:
            self.count += 1
            if self.count >= StateMachine.PROLOGUE_LEN:
                self.state = StateMachine.State.READ_CRC
                self.count = 0
        else:
            self.state = StateMachine.State.INIT
            self.count = 0

    def read_crc(self, c):
        self.msg_crc = c
        self.state = StateMachine.State.READ_LEN
        self.count = 0

    def read_len(self, c):
        self.msg_len = c
        self.state = StateMachine.State.READ_BODY
        self.count = 0
        self.body = bytearray()
        self.running_crc = 0

    def read_body(self, c):
        self.body.append(c)
        self.running_crc ^= c
        self.count += 1
        if self.count >= self.msg_len:
            self.state = StateMachine.State.EPILOGUE
            self.count = 0

    def epilogue(self, c):
        if c == StateMachine.EPILOGUE_CHAR:
            self.count += 1
            if self.count >= StateMachine.EPILOGUE_LEN:
                if self.msg_crc == self.running_crc:
                    logger.debug("Valid CRC")
                    self.writer.write(bytes(self.body))
                else:
                    logger.info("Bad CRC (expected: %i, actual: %i)", self.msg_crc, self.running_crc)
                self.state = StateMachine.State.INIT
                self.count = 0
        else:
            self.state = StateMachine.State.INIT
            self.count = 0

    def run(self):
        logger.debug("Parser running")
        while True:
            try:
                b = self.reader.read()
                c = ord(b)
            except Exception as e:
                logger.info("Exception: %s", e)
                return

            now = time.perf_counter()
            if self.state != StateMachine.State.INIT and self.last_ts > 0 and now - self.last_ts > StateMachine.SPEED_LIMIT:
                logger.info("Speed too low; Resetting.")
                self.state = StateMachine.State.INIT
                self.count = 0

            self.last_ts = now
            method = getattr(self, StateMachine.stateMap[self.state])
            logger.debug("State: %s(%02x)", self.state.name, c)
            method(c)
