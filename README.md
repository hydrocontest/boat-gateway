# Hydrocontest - Boat Gateway

The boat gateway receives information frames from the boat (using XBee/Bluetooth) through a USB/Serial connexion, decodes the protobuf, and publishes the result as JSON data to Redis.